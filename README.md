manual stuff:

this creates a random secret to use in the oauth2-proxy session

```bash
kubectl create secret generic proxy-secret --from-literal=session_secret=(openssl rand -base64 42)
```

"install" with kustomize from a checked out copy of this repo:
```
kubectl apply -k .
```

or
```
kubectl apply -k https://gitlab.oit.duke.edu/nbc9/okd-oauth-demo
```